FROM beamdog/nwserver:8193.32 as nwserver

FROM nimlang/nim:1.4.8 as nim
WORKDIR /tmp
RUN apt update \
    && apt install -y --no-install-recommends git bash \
    && git clone --recursive https://github.com/1d3s/neverwinter.nim  \
    && cd neverwinter.nim \
    && nimble build -d:release \
    && mv bin/* /usr/local/bin/



# following nim's base:
# https://github.com/moigagoo/nimage/blob/develop/choosenim/Dockerfile
FROM ubuntu:devel

RUN apt-get update \
    && apt-get install -y git-lfs wget zip p7zip-full rsync make jq moreutils dos2unix \
    && apt-get clean -y


RUN wget https://github.com/Beamdog/nwsync/releases/download/0.4.2/nwsync.linux.amd64.zip \
    && unzip nwsync.linux.amd64.zip \
    && mv nwsync_* /usr/local/bin/. \
    && rm nwsync.linux.amd64.zip


RUN wget https://github.com/nwneetools/nwnsc/releases/download/v1.1.2/nwnsc-linux-v1.1.2.zip \
    && unzip nwnsc-linux-v1.1.2.zip \
    && mv nwnsc /usr/local/bin/. \
    && rm nwnsc-linux-v1.1.2.zip

COPY --from=nim /usr/local/bin/* /usr/local/bin/
COPY --from=nwserver /nwn/data /nwn/data
